#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <SDL2/SDL_opengl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <Manager.hpp>
#include <FrameTime.hpp>
#include <Wavefront.hpp>
#include <Shader.hpp>
#include <random>
#include <ctime>

#ifdef __linux__
#include <unistd.h>
#endif//__linux__

#include <iostream>
#define DE_OUT( x ) std::cerr << "[main]  " << x << std::endl

SDL_Window* gWindow;

bool open_models( void );
bool close_assets( void );

int main( int argc, char* argv[] )
{
	srand( time( 0 ) );
	///SDL INITS
	if( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_TIMER ) > 0 )
	{
		DE_OUT( "SDL couldn't init\n" << SDL_GetError() );
		return 1;
	}
	SDL_StopTextInput();
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 2 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 1 );

	{
		int major, minor;
		SDL_GL_GetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, &major );
		SDL_GL_GetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, &minor );
		std::cerr << "[main]  GL Version#" << major << '.' << minor << std::endl;
	}
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8 );

	{
		gWindow = SDL_CreateWindow( "Notepad 3D", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 800, 600, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL );
		for( auto i = 1; i < argc; i++ )
		{
			if( argv[i][0] == '-' )
			{
				switch( argv[i][1] )
				{
					case 'h':
						std::cerr << R"(Type characters with your keyboard to write in the current document.
you can open files through command line or by setting this program as your default text editor. :^)
special commands follow:
CTRL-N = start a new document
CTRL-W = close the current document
CTRL-S = Saves files to said file name above the document
CTRL-O = Open inputted file
CTRL-J = change document -
CTRL-K = change document +
CTRL-P = print document to the command line
CTRL-Q = close application
CTRL-0 = reset camera position to 0
LEFT   = scroll the document to the left
RIGHT  = scroll the document to the right
UP     = scroll the document up
DOWN   = scroll the document down)" << std::endl;
						break;
					default: break;
				}//switch( argv[i][1] )
				continue;
			}
			Manager::make_document( argv[i] );
		}
		#ifdef __linux__
		if( not isatty(fileno(stdin)) )
		{
			Manager::make_document( std::cin );
		}
		#endif //__linux__

		//set window icon
		extern unsigned char Notepad_bmp;
		extern unsigned int Notepad_bmp_len;
		SDL_Surface *ico = SDL_LoadBMP_RW( SDL_RWFromMem( &Notepad_bmp, Notepad_bmp_len ), 1 );
		if( ico != nullptr )
		{
			SDL_SetWindowIcon( gWindow, ico );
			SDL_FreeSurface( ico );
		}
		else DE_OUT( "Couldn't load icon" );
	}
	SDL_GLContext wcontext = SDL_GL_CreateContext( gWindow );

	///OpenGL INITS
	glewExperimental = GL_TRUE;
	if( glewInit() > 0 )
	{
		DE_OUT( "GLEW couldn't init\n" << glewGetErrorString( glGetError() ) );
		return 3;
	}

	glEnable( GL_DEPTH_TEST );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	{
		int w, h;
		SDL_GetWindowSize( gWindow, &w, &h );
		glViewport( 0, 0, w, h );
	}

	if( SDL_GL_SetSwapInterval( -1 ) != 0 ) //vsync
	{
		DE_OUT( "Late swap not supported." );
		SDL_GL_SetSwapInterval( 1 );
	}

	///Assets INIT
	open_models();
	DE_OUT( "Loaded assets" );

	srand( time( nullptr ) );

	constexpr float MAX_FPS = 1000.0f/60.0f;
	DE_OUT( "Starting game loop" );

	gert::Wavefront::update_attribs();

	SDL_StartTextInput();
	while( Manager::alive() )
	{
		FrameTime::update();

		Manager::event_manage();

		glClearColor( 1.0f, 1.0f, 1.0f, 1.0f );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		Manager::draw();
		SDL_GL_SwapWindow( gWindow );

		if( FrameTime::since_update() < MAX_FPS )
		{
			SDL_Delay( MAX_FPS - FrameTime::since_update() );
		}
	}

	///deinit
	DE_OUT( "Exiting all" );
	close_assets();
	SDL_GL_DeleteContext( wcontext );
	SDL_DestroyWindow( gWindow );
	SDL_Quit();
	return 0;
}
