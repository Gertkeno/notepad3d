#include <Document.hpp>
#include <fstream>
#include <iostream>
#include <Shader.hpp>
#include <FrameTime.hpp>

namespace
{
	const Character ln( '=' );
}

Document::Document()
{
	_animationTimer = 0.0;
}

Document::Document( const char* filename )
{
	open( filename );
}

Document::Document( std::istream& file )
{
	open( file );
}

std::string Document::get_file()
{
	std::string file;
	for( auto &i: _characters )
	{
		file += i.code;
	}
	return file;
}

std::string Document::get_name()
{
	return _fileName;
}

bool Document::just_append( char val )
{
	_characters.push_back( val );
	return true;
}

bool Document::append_a_lot( const std::string& data )
{
	for( auto &i : data )
	{
		_characters.push_back( i );
	}
	return true;
}

bool Document::pop_back()
{
	if( _characters.size() > 0 )
	{
		_characters.pop_back();
		return true;
	}
	return false;
}

bool Document::empty()
{
	return _characters.size() == 0;
}

void Document::clear()
{
	_characters.clear();
	reset_camera();
}

void Document::draw()
{
	_animationTimer += FrameTime::get_mod();
	gert::Shader::Active_Shader->set_uniform( "offset", _camera );
	int xval(0);
	int yval(0);
	for( auto i = 0u; i < _renderFileName.size(); ++i )
	{
		_renderFileName[i].draw( xval, yval, _animationTimer );
		++xval;
	}
	xval=0;
	yval=2;
	bool rightOut(false);
	for( auto i = 0u; i < _characters.size(); ++i )
	{
		if( _characters[i] == '\n' )
		{
			++yval;
			xval=0;
			rightOut = false;
			continue;
		}
		if( -_camera.y + 2.1f < yval*SIZE*3 )
		{
			break;//bottom text detected
		}
		if( rightOut ) continue;
		if( _camera.x + 2.1f < xval*SIZE*2 )
		{
			rightOut = true;
		}
		_characters[i].draw( xval, yval, _animationTimer );
		++xval;
	}
	ln.draw( xval, yval, _animationTimer );
}

bool Document::save()
{
	if( _fileName.empty() )
	{
		std::cerr << "[Document] No filename" << std::endl;
		return false;
	}
	std::ofstream outFile( _fileName );
	if( not outFile.is_open() )
	{
		std::cerr << "[Document] Couldn't open file " << _fileName << std::endl;
		return false;
	}
	for( auto i = 0u; i < _characters.size(); ++i )
	{
		outFile.put( _characters[i].code );
	}
	if( _characters[_characters.size()-1].code != '\n' )
	{
		outFile.put( '\n' );
	}
	std::cerr << "[Document] Saved to file " << _fileName << std::endl;
	return true;
}

void Document::set_save( std::string fileNew )
{
	_fileName = fileNew;
	_renderFileName.clear();
	for( auto &i: _fileName )
	{
		_renderFileName.push_back( i );
	}
}

bool Document::open( std::string filename )
{
	std::ifstream file( filename );
	set_save( filename );
	if( not file.is_open() )
	{
		std::cerr << "[Document] Couldn't open file " << filename << std::endl;
		return false;
	}
	bool success = open( file );
	return success;
}

bool Document::open( std::istream& inme )
{
	char got;
	inme.get( got );
	while( not inme.eof() )
	{
		_characters.push_back( Character( got ) );
		inme.get( got );
	}
	return true;
}

void Document::reset_camera()
{
	_camera = { 0.0f, 0.0f, 0.0f };
}

void Document::modify_camera( float x, float y )
{
	_camera.x += x;
	_camera.y += y;
}
