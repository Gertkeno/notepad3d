#include <Character.hpp>

constexpr int not_random()
{
	return 0xFFFF7F;
}

constexpr bool test()
{
	static_assert( Character( 'p', not_random )=='p', "Operator == checks against character code" );
	static_assert( Character( '8', not_random ).model == model::Eight, "Numeral set incorrectly" );
	static_assert( Character( '!', not_random ).model == model::Bang, "Symbol set incorrectly" );
	static_assert( Character( '\a', not_random ).model == model::TOTAL, "Unknown symbol not set to TOTAL sentinel" );
	static_assert( Character( 'B', not_random ).model == model::B, "Capital letters are improperly found" );
}
