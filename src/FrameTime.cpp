#include <FrameTime.hpp>
#include <SDL2/SDL_timer.h>

namespace FrameTime
{
	namespace
	{
		double _pure;
		double _modified;
		double _modBuffer(1.0);
		unsigned _frameStart;
	}

	double get_pure()
	{
		return _pure;
	}

	double get_mod()
	{
		return _modified;
	}

	void set_mod( double set )
	{
		_modBuffer = set;
	}

	void update()
	{
		_pure = since_update() / 1000.0f;
		_modified = _pure * _modBuffer;
		_frameStart = SDL_GetTicks();
	}

	unsigned since_update()
	{
		return SDL_GetTicks() - _frameStart;
	}
}
