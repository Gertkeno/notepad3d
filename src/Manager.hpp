#ifndef MANAGER_H
#define MANAGER_H
#include <stddef.h>
#include <fstream>

namespace Manager
{
	size_t make_document( const char * filename );
	size_t make_document( std::istream& in );
	size_t active_document();
	size_t close_document();

	void event_manage();
	void draw();

	bool alive();
}

#endif //MANAGER_H
