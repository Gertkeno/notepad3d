#include <Shader.hpp>
#include <Wavefront.hpp>
#include <ModelList.hpp>
#include <GL/gl.h>

gert::Shader* gShaders;
gert::Wavefront* gModels;

bool open_models()
{
	gShaders = new gert::Shader;
	gShaders->open_shader( R"(
#version 130

in vec3 Norma;

void main()
{
	gl_FragColor = vec4 ( (Norma.r+1)/2, (Norma.g+1)/2, (Norma.b+1)/2, 1.0f );
}
)",  gert::Shader::FRAGMENT );
	gShaders->open_shader( R"(
#version 130

in vec3 position;
in vec2 texcoord;
in vec3 vnormal;

out vec3 Norma;
uniform mat4 model;
uniform vec3 offset;

void main()
{
	Norma = vnormal;
	vec4 rotPos = vec4( position.x, -1 * position.z, position.y, 1.0 );
	vec4 suboffset = model*rotPos;
	suboffset.x -= offset.x;
	suboffset.y -= offset.y;
	gl_Position = suboffset;
}
)", gert::Shader::VERTEX );
	gShaders->attach_shaders();
	gShaders->use_shader();

	gModels = new gert::Wavefront[model::TOTAL];
#define OPEN_LETTER( let )\
extern const float let##_vertices;\
extern const unsigned int let##_len;\
gModels[ model::let ].raw_vert_load( & let##_vertices, let##_len );

//welcome to macro
	OPEN_LETTER( A ); OPEN_LETTER( B ); OPEN_LETTER( C );
	OPEN_LETTER( D ); OPEN_LETTER( E ); OPEN_LETTER( F );
	OPEN_LETTER( G ); OPEN_LETTER( H ); OPEN_LETTER( I );
	OPEN_LETTER( J ); OPEN_LETTER( K ); OPEN_LETTER( L );
	OPEN_LETTER( M ); OPEN_LETTER( N ); OPEN_LETTER( O );
	OPEN_LETTER( P ); OPEN_LETTER( Q ); OPEN_LETTER( R );
	OPEN_LETTER( S ); OPEN_LETTER( T ); OPEN_LETTER( U );
	OPEN_LETTER( V ); OPEN_LETTER( W ); OPEN_LETTER( X );
	OPEN_LETTER( Y ); OPEN_LETTER( Z );
//special character
	OPEN_LETTER( Bang );
	OPEN_LETTER( Dash );
	OPEN_LETTER( Dollar );
	OPEN_LETTER( Equals );
	OPEN_LETTER( Period );
	OPEN_LETTER( Ques );
	OPEN_LETTER( FSlash );
	OPEN_LETTER( Comma );
	OPEN_LETTER( Apos );
//numbers
	OPEN_LETTER( Zero );
	OPEN_LETTER( One );
	OPEN_LETTER( Two );
	OPEN_LETTER( Three );
	OPEN_LETTER( Four );
	OPEN_LETTER( Five );
	OPEN_LETTER( Six );
	OPEN_LETTER( Seven );
	OPEN_LETTER( Eight );
	OPEN_LETTER( Nine );
	return true;
}
bool close_assets()
{
	delete gShaders;
	return false;
}
