#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <vector>
#include <string>
#include <Character.hpp>
#include <glm/glm.hpp>

class Document
{
	public:
		Document();
		Document( const char* filename );
		Document( std::istream& file );
		
		std::string get_file();
		bool just_append( char val );
		bool append_a_lot( const std::string& data );
		bool pop_back();
		bool empty();
		void clear();

		void draw();

		void reset_camera();
		void modify_camera( float x, float y );

		bool save();
		void set_save( std::string nextFileName );
		bool open( std::string filename );
		bool open( std::istream& file );
		std::string get_name();
	private:
		std::vector<Character> _characters;
		glm::vec3 _camera;
		std::vector<Character> _renderFileName;
		std::string _fileName;
		double _animationTimer;
};

#endif //DOCUMENT_H
