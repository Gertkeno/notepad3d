#include <Character.hpp>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <Wavefront.hpp>

void Character::draw( int x, int y, float anime ) const
{
	if( model < 0 or model >= model::TOTAL ) return;
	constexpr float TAO{ M_PI*2.0f };
	const float xr( -1.0f + SIZE + (SIZE*2*x) );
	const float yr( 1.0f - SIZE - (SIZE*3*y) );
	glm::mat4 at = glm::translate( glm::mat4(), glm::vec3( xr, yr, 0.0f ) );
	at = glm::scale( at, glm::vec3( SIZE ) );
	if( animationStyle == ROTATE )
	{
		at = glm::rotate( at, (anime*animationMax)*TAO, animationWeight );
	}
	else if( animationStyle == SCALE )
	{
		const float normanime{ anime*animationMax };
		const float xr( animationWeight.x * cos( normanime*TAO ) );
		const float yr( animationWeight.y * sin( normanime*TAO ) );
		at = glm::scale( at, glm::vec3( 1.0f - xr, 1.0f - yr, 1.0f ) );
	}
	gModels[ model ].draw( at, nullptr, color::WHITE );
}
