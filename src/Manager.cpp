#include <Manager.hpp>
#include <Document.hpp>
#include <vector>
#include <iostream>
#include <FrameTime.hpp>
#include <SDL2/SDL.h>
#include <GL/gl.h>

#define ACTIVE_COUNT _activeDoc << "/" << _docs.size()

extern SDL_Window* gWindow;

namespace
{
	std::vector<Document> _docs(1);
	size_t _activeDoc(0);
	SDL_Event _event;
	bool living(true);

	enum keydown_t: unsigned char
	{
		LEFT  = 1 << 0,
		RIGHT = 1 << 1,
		DOWN  = 1 << 2,
		UP    = 1 << 3,
	};
	unsigned char _keyDown(0);

	Document _fileMaker;
	enum state_t
	{
		FILENAME,
		APPEND,
		TOTAL
	} _writeState( APPEND );
}

namespace Manager
{
	void set_title( std::string title )
	{
		SDL_SetWindowTitle( gWindow, std::string( "Notepad 3D - " + title ).c_str() );
	}

	size_t make_document( const char * filename )
	{
		if( filename != nullptr )
		{
			if( _docs[_activeDoc].empty() ) _docs[_activeDoc].open( filename );
			else _docs.push_back( Document( filename ) );
		}
		else _docs.push_back( Document() );
		_activeDoc = _docs.size()-1;
		if( filename == nullptr )
			std::cerr << "[Manager] New empty document " << "(" << ACTIVE_COUNT << ")" << std::endl;
		else
			std::cerr << "[Manager] New document of " << filename << " (" << ACTIVE_COUNT << ")" << std::endl;
		set_title( _docs[_activeDoc].get_name() );
		return _activeDoc;
	}

	size_t make_document( std::istream& in )
	{
		if( _docs[_activeDoc].empty() ) _docs[_activeDoc].open( in );
		else _docs.push_back( Document( in ) );
		_activeDoc = _docs.size()-1;
		std::cerr << "[Manager] New document from istream" << std::endl;
		set_title( _docs[_activeDoc].get_name() );
		return _activeDoc;
	}

	size_t active_document()
	{
		return _activeDoc;
	}

	size_t close_document()
	{
		if( _docs.size() <= 1 ) return _activeDoc;
		_docs.erase(_docs.begin()+_activeDoc);
		std::cerr << "[Manager] Closed document (" << ACTIVE_COUNT << ")" << std::endl;
		_activeDoc = _docs.size()-1;
		set_title( _docs[_activeDoc].get_name() );
		return _activeDoc;
	}

	void event_manage()
	{
		while( SDL_PollEvent( &_event ) )
		{
			switch( _event.type )
			{
				case SDL_WINDOWEVENT:
					if( _event.window.event == SDL_WINDOWEVENT_RESIZED )
					{
						int w,h;
						SDL_GetWindowSize( gWindow, &w, &h );
						glViewport( 0, 0, w, h );
					}
					break;
				case SDL_QUIT:
					living = false;
					break;
				case SDL_TEXTINPUT:
					if( SDL_GetModState() & KMOD_LCTRL ) break;
					if( _writeState == FILENAME )
						_fileMaker.append_a_lot( _event.text.text );
					else if( _writeState == APPEND )
						_docs[_activeDoc].append_a_lot( _event.text.text );

					break;
				case SDL_KEYDOWN:
					if( _event.key.keysym.sym == SDLK_BACKSPACE )
					{
						if( _writeState == FILENAME )
							_fileMaker.pop_back();
						else if( _writeState == APPEND )
							_docs[_activeDoc].pop_back();
					}
					else if( _event.key.keysym.sym == SDLK_RETURN )
					{
						if( _writeState == FILENAME )
						{
							make_document( _fileMaker.get_file().c_str() );
							_fileMaker.clear();
							_writeState = APPEND;
						}
						else if( _writeState == APPEND )
							_docs[_activeDoc].just_append( '\n' );
					}
					else if( _event.key.keysym.sym == SDLK_RIGHT )
						_keyDown = _keyDown|RIGHT;
					else if( _event.key.keysym.sym == SDLK_LEFT)
						_keyDown = _keyDown|LEFT;
					else if( _event.key.keysym.sym == SDLK_UP )
						_keyDown = _keyDown|UP;
					else if( _event.key.keysym.sym == SDLK_DOWN )
						_keyDown = _keyDown|DOWN;

					if( (SDL_GetModState() & KMOD_LCTRL) != KMOD_LCTRL ) break;
					if( _event.key.keysym.sym == SDLK_p )
					{
						std::cerr << "[Manager] Reading from active document(" << ACTIVE_COUNT << ", " << _docs[_activeDoc].get_name() <<"):";
						std::cout << _docs[_activeDoc].get_file() << std::endl;
					}
					else if( _event.key.keysym.sym == SDLK_q )
						living = false;
					else if( _event.key.keysym.sym == SDLK_n )
						make_document( nullptr );
					else if( _event.key.keysym.sym == SDLK_w )
						close_document();
					else if( _event.key.keysym.sym == SDLK_j )
					{
						if( _activeDoc <= 0 )break;
						--_activeDoc;
						std::cerr << "[Manager] Down to document " << ACTIVE_COUNT << ", " << _docs[_activeDoc].get_name() << std::endl;
						set_title( _docs[_activeDoc].get_name() );
					}
					else if( _event.key.keysym.sym == SDLK_k )
					{
						if( _activeDoc >= _docs.size()-1 ) break;
						++_activeDoc;
						std::cerr << "[Manager] Up to document " << ACTIVE_COUNT << ", " << _docs[_activeDoc].get_name() << std::endl;
						set_title( _docs[_activeDoc].get_name() );
					}
					else if( _event.key.keysym.sym == SDLK_s )
						_docs[_activeDoc].save();
					else if( _event.key.keysym.sym == SDLK_v )
						_docs[_activeDoc].append_a_lot( SDL_GetClipboardText() );
					else if( _event.key.keysym.sym == SDLK_o )
					{
						_fileMaker.set_save( "Enter File Name:" );
						_writeState = FILENAME;
					}
					else if( _event.key.keysym.sym == SDLK_0 )
						_docs[_activeDoc].reset_camera();
					else if( _event.key.keysym.sym == SDLK_BACKSPACE )
					{
						std::string dat = _docs[_activeDoc].get_file();
						auto len = dat.rfind(' ');
						if( len >= std::string::npos )
						{
							break;
						}
						len = _docs[_activeDoc].get_file().size() - len;
						for( auto i = 0u; i < len; ++i )
						{
							_docs[_activeDoc].pop_back();
						}
					}
					break;
				case SDL_KEYUP:
					if( _event.key.keysym.sym == SDLK_RIGHT )
						_keyDown = _keyDown& (~RIGHT);
					else if( _event.key.keysym.sym == SDLK_LEFT)
						_keyDown = _keyDown& (~LEFT);
					else if( _event.key.keysym.sym == SDLK_UP )
						_keyDown = _keyDown& (~UP);
					else if( _event.key.keysym.sym == SDLK_DOWN )
						_keyDown = _keyDown& (~DOWN);
					break;
			}
		}
	}

	bool alive()
	{
		return living;
	}

	void draw()
	{
		if( _writeState == FILENAME )
		{
			_fileMaker.draw();
			return;
		}
		if( _keyDown & LEFT )
			_docs[_activeDoc].modify_camera( -0.5f * FrameTime::get_pure(), 0.0f );
		else if( _keyDown & RIGHT )
			_docs[_activeDoc].modify_camera( 0.5f * FrameTime::get_pure(), 0.0f );
		if( _keyDown & UP )
			_docs[_activeDoc].modify_camera( 0.0f, 0.5f * FrameTime::get_pure() );
		else if( _keyDown & DOWN )
			_docs[_activeDoc].modify_camera( 0.0f, -0.5f * FrameTime::get_pure() );
		_docs[_activeDoc].draw();
	}
}
