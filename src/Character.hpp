#ifndef CHARACTER_H
#define CHARACTER_H

#include <glm/glm.hpp>
#include <random>
#include <ModelList.hpp>

class Character
{
	private:
		static constexpr float RANDF = RAND_MAX;
		static constexpr float RECIP_RANDF{ 1.0f / RANDF };

		static constexpr int char_to_model( char c )
		{
			const int index( c >= 'a' ? c - 'a' : c - 'A' );
			if( index <= model::Z and index >= 0 )
				return index;

			//numbers
			const int temp( c - '0' );
			if( temp >= 0 and temp <= 9 )
				return temp + model::Zero;

			//special character check
			constexpr std::pair<char, model::storage> data[]{
				{'!',model::Bang},
				{'-',model::Dash},
				{'_',model::Dash},
				{'$',model::Dollar},
				{'=',model::Equals},
				{'.',model::Period},
				{'?',model::Ques},
				{'/',model::FSlash},
				{',',model::Comma},
				{'\'',model::Apos},
				{'`',model::Apos}
			};

			for( auto & i : data )
			{
				if( c == i.first )
					return i.second;
			}

			//not found
			return model::TOTAL;
		}

		float animationMax;
		enum animate_t: unsigned char
		{
			ROTATE,
			SCALE,
			//NONE,
			TOTAL
		} animationStyle;
		glm::vec3 animationWeight;
	public:
		constexpr Character( char val, int(*r)() = rand ):
		code{ val }
		,animationStyle{ animate_t(r()%animate_t::TOTAL) }
		,animationMax{ 1.0f / ((r()*RECIP_RANDF*9) + 1.0f) }
		,animationWeight{ r()*RECIP_RANDF, r()*RECIP_RANDF, r()*RECIP_RANDF }
		,model{ char_to_model(val) }
		{}
		void draw( int x, int y, float anime ) const;

		char code;
		int model;
		constexpr Character operator=( char v )
		{
			code = v;
			model = char_to_model(v);
			return * this;
		}

		constexpr bool operator==( char v ) const
		{
			return code == v;
		}
};

constexpr float SIZE = 0.029f;

#endif //CHARACTER_H
