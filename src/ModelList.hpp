#ifndef MODELLIST_H
#define MODELLIST_H

namespace gert
{
	class Wavefront;
	class Texture;
	class Shader;
	class Font;
}
typedef unsigned char uint8_t;

namespace model
{
	enum storage: uint8_t
	{
		A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
		Bang, Dash, Dollar, Equals, Period, Ques, FSlash, Comma, Apos,
		Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine,
		TOTAL
	};
}
extern gert::Wavefront* gModels;
extern gert::Shader* gShaders;

#endif // MODELLIST_H
