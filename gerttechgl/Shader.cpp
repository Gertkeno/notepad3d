#include "Shader.hpp"

#include <SDL2/SDL_rwops.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

static const char* FILE_ERROR_STRING = "File could not be opened/read: ";
static const uint8_t SHADER_RECENT_MAX = 30;
static const uint8_t SHADER_RECENT_STR = 17;//total character, keep uniforms under SHADER_RECENT_STR characters long
static const uint8_t SHADER_ERROR_MAX = 0xFF;

using namespace gert;

Shader* Shader::Active_Shader = nullptr;
Shader::Shader( void )
{
	_shaders = new GLuint[ TOTAL ];
	_program = new GLuint;
	*_program = 0;

	_errorLog = nullptr;

	_lastLoc = 0;
	_recentLoc = new int[ SHADER_RECENT_MAX ];
	_recentUniform = new char*[ SHADER_RECENT_MAX ];
	for( uint8_t i = 0; i < SHADER_RECENT_MAX; i++ )
	{
		_recentUniform[i] = new char[ SHADER_RECENT_STR ];
		for( uint8_t a = 0; a < SHADER_RECENT_STR; a++ )
		{
			_recentUniform[i][a] = '\0';
		}
	}
}

Shader::~Shader( void )
{
	destroy_me(); 
	delete[] _shaders;
	delete[] _recentUniform;
	delete _program;

	if( _errorLog != nullptr )
	{
		delete[] _errorLog;
	}
}

bool Shader::open_shader_file( const char* fileName, shadeType t )
{
	if( t == TOTAL )
	{
		GLuint i = 0;
		while( fileName[ i ] != '\0' )
		{
			if( fileName[ i++ ] != '.' ) continue;
			switch( fileName[ i ] )
			{
			case 'v':
				t = VERTEX;
				break;
			case 'f':
				t = FRAGMENT;
				break;
			}
			break;
		}
	}

	SDL_RWops* shadSource = SDL_RWFromFile( fileName, "r" );
	if( shadSource != nullptr && SDL_RWsize( shadSource ) > 0 )
	{
        unsigned int shadSize = SDL_RWsize( shadSource );
		GLchar* textBuffer = new GLchar[ shadSize + 1 ];
		SDL_RWread( shadSource, textBuffer, sizeof( GLchar ), shadSize );
		textBuffer[ shadSize ] = '\0';
		SDL_RWclose( shadSource );

		bool rval = open_shader( textBuffer, t );
		delete[] textBuffer;
		return rval;
	}
	else
	{
		if( _errorLog == nullptr )
		{
			_errorLog = new char[ SHADER_ERROR_MAX ];
		}
		unsigned int i = 0;
		while( FILE_ERROR_STRING[i] != '\0' )
		{
			_errorLog[ i ] = FILE_ERROR_STRING[ i ];
			_errorLog[ i + 1 ] = '\0';
			i++;
		}

		for( unsigned int b = 0; fileName[ b ] != '\0'; b++ )
		{
			_errorLog[ i + b ] = fileName[ b ];
			_errorLog[ i + b + 1 ] = '\0';
		}
	}
	return false;
}

bool Shader::attach_shaders( void )
{
	*_program = glCreateProgram();
	glAttachShader( *_program, _shaders[ FRAGMENT ] );
	glAttachShader( *_program, _shaders[ VERTEX ] );

	glLinkProgram( *_program );
	return true;
}

bool Shader::open_shader_file( const char* filename0, const char* filename1 )
{
	bool good = ( open_shader_file( filename0 ) && open_shader_file( filename1 ) );
	if( good )
		attach_shaders();

	return good;
}

bool Shader::open_shader( const char* source, shadeType t )
{
	if( source == nullptr or t == TOTAL )
		return false;

	_shaders[ t ] = glCreateShader( GL_FRAGMENT_SHADER + t );
	glShaderSource( _shaders[ t ], 1, &source, nullptr );
	glCompileShader( _shaders[ t ] );

	GLint status;
	glGetShaderiv( _shaders[ t ], GL_COMPILE_STATUS, &status );
	if( status != GL_TRUE )
	{
		if( nullptr == _errorLog )
		{
			_errorLog = new char[ SHADER_ERROR_MAX ];
		}
		glGetShaderInfoLog( _shaders[ t ], SHADER_ERROR_MAX, nullptr, _errorLog );
		std::cerr << _errorLog << std::endl;
		return false;
	}
	return true;
}

void Shader::use_shader( void )
{
	glUseProgram( *_program );
	Active_Shader = this;
}

void Shader::destroy_me( void )
{
	glDeleteProgram( *_program );
	glDeleteShader( _shaders[ FRAGMENT ] );
	glDeleteShader( _shaders[ VERTEX ] );
	if( Active_Shader == this )
	{
		Active_Shader = nullptr;
	}
}

const char* Shader::get_error_log( void )
{
	return _errorLog;
}

GLuint Shader::get_program( void )
{
	return *_program;
}

void Shader::set_uniform( const char* title, float value )
{
	glProgramUniform1f( *_program, get_location( title ), value );
}

void Shader::set_uniform( const char* title, int value )
{
	glProgramUniform1i( *_program, get_location( title ), value );
}

void Shader::set_uniform( const char* title, GLuint value )
{
	glProgramUniform1ui( *_program, get_location( title ), value );
}

void Shader::set_uniform( const char* title, glm::vec3 value )
{
	glProgramUniform3fv( *_program, get_location( title ), 1, glm::value_ptr( value ) );
}

void Shader::set_uniform( const char* title, glm::vec4 value )
{
	glProgramUniform4fv( *_program, get_location( title ), 1, glm::value_ptr( value ) );
}

int Shader::get_location( const char* title )
{
	for( uint8_t i = 0; i < SHADER_RECENT_MAX; i++ )
	{
		if( strcmp( title, _recentUniform[i] ) == 0 ) return _recentLoc[i];
	}

	if( _lastLoc >= SHADER_RECENT_MAX ) _lastLoc = 0;
	for( uint8_t i = 0; i < SHADER_RECENT_STR && title[ i ] != '\0'; i++ )
	{
		_recentUniform[_lastLoc][ i ] = title[ i ];
		_recentUniform[_lastLoc][ i+1 ] = '\0';
	}

	return ( _recentLoc[_lastLoc++] = glGetUniformLocation( *_program, title ) );
}
