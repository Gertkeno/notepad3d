#ifndef GERT_SHADER_H
#define GERT_SHADER_H

typedef unsigned int GLuint;

#include <glm/glm.hpp>

namespace gert
{
class Shader
{
public:
    Shader( void );
    ~Shader( void );

    enum shadeType: unsigned char
    {
        FRAGMENT,
        VERTEX,//keep these in order
        TOTAL
    };

    static Shader* Active_Shader;

    bool open_shader_file( const char* filename, shadeType t = TOTAL );///< if default checks fragment/vertex shader based on .f* / .v*
    bool open_shader_file( const char* filename0, const char* filename1 );///< runs single file loader twice and executes attach_shaders if done correctly
	bool open_shader( const char* source, shadeType );
    bool attach_shaders( void );
    void use_shader( void );
    void destroy_me( void );

    void set_uniform( const char* title, float value );
    void set_uniform( const char* title, int value );
    void set_uniform( const char* title, GLuint value );
    void set_uniform( const char* title, glm::vec3 value );
    void set_uniform( const char* title, glm::vec4 value );

    const char* get_error_log( void );
    GLuint get_program( void );
private:
    int get_location( const char* title );

    int* _recentLoc, _lastLoc;
    char** _recentUniform;
    GLuint* _shaders;
    GLuint* _program;

    char* _errorLog;
};
}

#endif // GERT_SHADER_H
