incls="-I ../src/ -I ../gerttechgl/ "
defs="-D GLM_FORCE_RADIANS -D SDL_MAIN_HANDLED -D GLEW_STATIC -D _GERT_DEBUG"
misc="-O2 -ggdb -Wall -std=gnu++14"
dest="obj/"

args="$defs $incls $misc"
