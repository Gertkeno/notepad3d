# Use
Open a document with this application to edit it. CTRL-s to save CTRL-o to open a different file.

With multiple files open use CTRL-j and CTRL-k to change the working file. CTRL-w to close the working file.

Arrow keys move the camera around, the cursor is always at the very end of the document.

# Windows
Check [downloads](../../downloads/) for a windows binary.

# Linux
## required libraries
- [SDL2](https://www.libsdl.org/projects/SDL_ttf/)
- [glm](https://glm.g-truc.net/0.9.8/index.html)
- [GLEW](http://glew.sourceforge.net/)

## helpful
- [cmake](https://cmake.org/)

## using apt
`apt install cmake libsdl2-dev libglew-dev libglm-dev`

## building
`mkdir build ; cd build/`
`cmake ../ && make`

run `notepad` executable, should be located in `build/` directory
